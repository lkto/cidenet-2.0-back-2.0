import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { User } from 'src/models/user.interface';
import { UserModule } from 'src/modules/user/user.module';
import { AreaService } from 'src/user/services/area/area.service';
import { UserService } from 'src/user/services/user/user.service';

@Controller('user')
export class UserController {
  constructor(
    private userService: UserService,
    private areaService: AreaService,
  ) {}

  @Get()
  getUsers(): any {
    return this.userService.findAll();
  }

  @Post()
  addUsers(@Body() user: User): any {
    return this.userService.validateUser(user);
  }

  @Put(':id')
  updateProduct(@Body() user: User, @Param() params: any): any {
    return this.userService.updateUser(params.id, user);
  }

  @Delete(':id')
  deleteUser(@Param() params: any): any {
    return this.userService.deleteUser(params.id);
  }

  @Get(':id')
  getOneUser(@Param() params: any): any {
    return this.userService.findOneUser(params.id);
  }
}
