import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { CountryService } from 'src/user/services/country/country.service';

@Controller('country')
export class CountryController {
  constructor(private countryService: CountryService) {}

  @Get()
  getCountries(): any {
    return this.countryService.findAll();
  }
}
