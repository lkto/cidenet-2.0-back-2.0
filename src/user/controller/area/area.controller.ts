import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { AreaService } from 'src/user/services/area/area.service';

@Controller('area')
export class AreaController {
  constructor(private areaService: AreaService) {}

  @Get()
  getAreas(): any {
    return this.areaService.findAll();
  }
}
