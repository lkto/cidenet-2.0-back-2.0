import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CountryEntity } from 'src/modules/user/entity/country-entity';
import { Repository } from 'typeorm';

@Injectable()
export class CountryService {
  constructor(
    @InjectRepository(CountryEntity)
    private readonly countryRP: Repository<CountryEntity>,
  ) {
    this.countryRP = countryRP;
  }

  async findOneCountry(id: number) {
    return await this.countryRP.findOne({
      where: {
        id_country: id,
      },
    });
  }

  async findAll() {
    return await this.countryRP.find();
  }
}
