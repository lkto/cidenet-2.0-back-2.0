import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Console } from 'console';
import { User } from 'src/models/user.interface';
import { UserEntity } from 'src/modules/user/entity/user-entity';
import { Repository } from 'typeorm';
import { validate } from 'validate.js';
import { AreaService } from '../area/area.service';
import { CountryService } from '../country/country.service';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly userRP: Repository<UserEntity>,
    private readonly areaService: AreaService,
    private readonly countryService: CountryService,
  ) {
    this.userRP = userRP;
    this.areaService = areaService;
    this.countryService = countryService;
  }

  async saveUser(user: any) {
    await this.userRP.insert(user);
    return user;
  }

  async updateUser(id: number, user: any) {
    await this.userRP.update(id, user);
    return user;
  }

  async findAll() {
    return await this.userRP.find();
  }

  async findOneUser(id: number) {
    return await this.userRP.findOne({
      where: {
        id: id,
      },
    });
  }

  async findOneByDocument(documento: string) {
    return await this.userRP.findOne({
      where: {
        number: documento,
      },
    });
  }

  async findOneByEmail(email: string) {
    return await this.userRP.findOne({
      where: {
        email: email,
      },
    });
  }

  async deleteUser(id: number) {
    return await this.userRP.delete(id);
  }

  async validateUser(user: User) {
    let error = false;
    let countError = 0;
    const msg = [];
    if (user.first_last_name == '') {
      error = true;
      countError += 1;
      msg.push('El primer apellido es obligatorio');
    } else {
      if (user.first_last_name.length > 20) {
        error = true;
        countError += 1;
        msg.push('El primer apellido solo puede tener 20 caracteres');
      }
    }

    if (user.second_last_name == '') {
      error = true;
      countError += 1;
      msg.push('El segundo apellido es obligatorio');
    } else {
      if (user.second_last_name.length > 20) {
        error = true;
        countError += 1;
        msg.push('El segundo apellido solo puede tener 20 caracteres');
      }
    }

    if (user.first_name == '') {
      error = true;
      countError += 1;
      msg.push('El primer nombre es obligatorio');
    } else {
      if (user.second_last_name.length > 20) {
        error = true;
        countError += 1;
        msg.push('El primer nombre solo puede tener 20 caracteres');
      }
    }

    if (user.other_name != '') {
      if (user.other_name.length > 50) {
        error = true;
        countError += 1;
        msg.push('El segundo nombre solo puede tener 50 caracteres');
      }
    }

    if (user.number == '') {
      error = true;
      countError += 1;
      msg.push('El documento es obligatorio');
    } else {
      if (user.second_last_name.length > 20) {
        error = true;
        countError += 1;
        msg.push('El documento solo puede tener 20 caracteres');
      }
    }

    await this.findOneByDocument(user.number).then((user: User) => {
      if (user) {
        error = true;
        countError += 1;
        msg.push('El usuario con dicho documento ya se encuentra registrado');
      }
    });

    await this.findOneByDocument(user.number).then((user: User) => {
      if (user) {
        error = true;
        countError += 1;
        msg.push('El usuario con dicho documento ya se encuentra registrado');
      }
    });

    const country = await this.countryService.findOneCountry(user.id_country);
    let domain = 'cidenet.com.co';
    if (country) {
      if (country.iso_code == 'USA') {
        domain = 'cidenet.com.us';
      }
    }

    if (error) {
      return {
        error: error,
        count: countError,
        msg: msg,
      };
    } else {
      const newUser = await this.saveUser(user);
      let userEmail =
        newUser.first_name + '.' + newUser.first_last_name + '@' + domain;

      const userEmailObject = await this.findOneByEmail(userEmail);

      if (userEmailObject) {
        userEmail =
          newUser.first_name +
          '.' +
          newUser.first_last_name +
          '.' +
          newUser.id +
          '@' +
          domain;
      }

      newUser.email = userEmail;

      return await this.updateUser(newUser.id, newUser);
    }
  }
}
