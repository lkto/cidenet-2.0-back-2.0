import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AreaEntity } from 'src/modules/user/entity/area-entity';
import { Repository } from 'typeorm';

@Injectable()
export class AreaService {
  constructor(
    @InjectRepository(AreaEntity)
    private readonly areaRP: Repository<AreaEntity>,
  ) {
    this.areaRP = areaRP;
  }

  async findOneArea(id: number) {
    return await this.areaRP.findOne({
      where: {
        id_area: id,
      },
    });
  }

  async findAll() {
    return await this.areaRP.find();
  }
}
