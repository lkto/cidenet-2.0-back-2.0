import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DocumentEntity } from 'src/modules/user/entity/document-entity';
import { Repository } from 'typeorm';

@Injectable()
export class DocumentService {
  constructor(
    @InjectRepository(DocumentEntity)
    private readonly documentRP: Repository<DocumentEntity>,
  ) {
    this.documentRP = documentRP;
  }

  async findAll() {
    return await this.documentRP.find();
  }
}
