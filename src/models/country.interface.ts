export interface Country {
  id_country: number;
  name: string;
  iso_code: string;
}
