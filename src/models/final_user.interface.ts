import { Area } from './area.interface';

export interface FinalUser {
  id: number;
  first_name: string;
  other_name: string;
  first_last_name: string;
  second_last_name: string;
  id_country: number;
  id_document: number;
  number: string;
  email: string;
  date_ingress: Date;
  area: Area;
  status: string;
  date_register: Date;
}