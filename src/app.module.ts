import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DatabaseModule } from './modules/database/database.module';
import { UserModule } from './modules/user/user.module';
import { UserService } from './user/services/user/user.service';
import { UserController } from './user/controller/user/user.controller';
import { UserEntity } from './modules/user/entity/user-entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AreaEntity } from './modules/user/entity/area-entity';
import { CountryEntity } from './modules/user/entity/country-entity';
import { DocumentEntity } from './modules/user/entity/document-entity';
import { AreaService } from './user/services/area/area.service';
import { CountryService } from './user/services/country/country.service';
import { CountryController } from './user/controller/country/country.controller';
import { AreaController } from './user/controller/area/area.controller';
import { DocumentService } from './user/services/document/document.service';
import { DocumentController } from './user/controller/document/document.controller';

@Module({
  imports: [
    DatabaseModule,
    UserModule,
    TypeOrmModule.forFeature([
      UserEntity,
      AreaEntity,
      CountryEntity,
      DocumentEntity,
    ]),
  ],
  controllers: [
    AppController,
    UserController,
    CountryController,
    AreaController,
    DocumentController,
  ],
  providers: [
    AppService,
    UserService,
    AreaService,
    CountryService,
    DocumentService,
  ],
})
export class AppModule {}
