import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity('area')
export class AreaEntity {
  @PrimaryGeneratedColumn()
  id_area: number;

  @Column()
  name: string;
}
