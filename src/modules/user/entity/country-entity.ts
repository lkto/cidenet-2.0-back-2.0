import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity('country')
export class CountryEntity {
  @PrimaryGeneratedColumn()
  id_country: number;

  @Column()
  name: string;

  @Column()
  iso_code: string;
}
