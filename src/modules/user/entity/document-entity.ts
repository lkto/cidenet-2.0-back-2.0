import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity('document')
export class DocumentEntity {
  @PrimaryGeneratedColumn()
  id_document: number;

  @Column()
  name: string;
}
