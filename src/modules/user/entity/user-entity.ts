import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity('user')
export class UserEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  first_name: string;

  @Column()
  other_name: string;

  @Column()
  first_last_name: string;

  @Column()
  second_last_name: string;

  @Column()
  id_country: number;

  @Column()
  id_document: number;

  @Column()
  number: string;

  @Column()
  email: string;

  @Column()
  date_ingress: Date;

  @Column()
  id_area: number;

  @Column()
  status: string;

  @Column()
  date_register: Date;
}
