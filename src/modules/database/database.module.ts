import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Connection } from 'typeorm';
import { AreaEntity } from '../user/entity/area-entity';
import { CountryEntity } from '../user/entity/country-entity';
import { DocumentEntity } from '../user/entity/document-entity';
import { UserEntity } from '../user/entity/user-entity';
import { UserModule } from '../user/user.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: '',
      database: 'db_cidenet_test',
      entities: [UserEntity, AreaEntity, DocumentEntity, CountryEntity],
      synchronize: true,
    }),
    UserModule,
  ],
})
export class DatabaseModule {
  constructor(private readonly connection: Connection) {}
}
